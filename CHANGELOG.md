# [1.5.0](/Shell_Snippets/cleanKernel/compare/1.5.0...main)
* Improvement in order to cover the issue #10, output format aspects
* Removed `at` from required packages because currently is not necessary

# [1.4.0](/Shell_Snippets/cleanKernel/compare/1.4.0...main)
* Improvement the python code to be more readable
* Improvement the program options explanation
* Adding *q* option when upgrade the packages for unattended updgrades

# [1.3.0](/Shell_Snippets/cleanKernel/compare/1.3.0...main)
* CICD migraton from Jenkins to GitlabCI
* Improvement on `.gitignore` file

# [1.2.0](/Shell_Snippets/cleanKernel/compare/1.2.0...main)
* Se crean un par de clases para la gestión específica de la configuración del programa y actualizaciones
* Optimización y pequeñas mejoras en la lógica del código del programa
* Se mejora la documentación del programa
* Se genera *helper* con datos para recuperar el provisionamiento del proyecto

# [1.1.0](/Shell_Snippets/cleanKernel/compare/1.1.0...main)
* Se aplica linter `pylint` a los 2 módulos así mismo, se deja el fichero de configuración **.pylintrc**
* Se actualiza la *pipeline* para hacer el chequeo de **pylint** sobre los módulos python.

# [1.0.0](/Shell_Snippets/cleanKernel/compare/1.0.0...main)
* Se cambia el código a lenguaje `python`

# [0.4.0](/Shell_Snippets/cleanKernel/compare/0.4.0...main)
* Se agrega tester para Jenkins, se procederá a seguir con un estándar para evaluar código.
* Se aplican algunas mejoras en el código para validar procesos y flexibilizar caracteristicas.

# [0.3.2](/Shell_Snippets/cleanKernel/compare/0.3.2...main)
* Se corrige el linter para comprobar paquetes instalados

# [0.3.1](/Shell_Snippets/cleanKernel/compare/0.3.1...main)
* Se da inicio al *CHANGELOG*
* Se ajusta el instalador para que pueda recibir argumentos
