cleanKernel
===================

Description
-------------
This project allow to manage easily the operating system package and help to keep **space-free** on your */boot* directory deleting *old kernels*, it's necessary that you run with **root user**.

Requirements
-------------
The program require of `dpkg` and `apt` as package management to work properly.

Installation
-------------
You can install the program on your computer running this command:

    ln -s /foo/bar/clean_kernel.py /usr/bin/clean_kernel

How to use
-------------
Just run the command:

    /foo/bar/clean_kernel.py

You can use the **--help** option to check the argments supported, by example:

```
usage: clean_kernel.py [-h] [--action {update,check,clean}]

This program allow you to manage easily your operating system packages

options:
  -h, --help            show this help message and exit
  --action {update,check,clean}
                        Fill a valid action, by example: check
