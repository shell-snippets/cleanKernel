#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program alow to do maintenance task to the package system manager
also allow to keep a limit of the kernel installed on the system
"""

import argparse
import os
import subprocess
import sys

# Fetch arguments passed to script
parser = argparse.ArgumentParser(
    description="This program allow you to manage easily your operating system upgrades"
)
parser.add_argument(
    "--action",
    help="Fill a valid action, by example: check",
    choices=["update", "check", "clean"],
    default="check",
)
args = parser.parse_args()


# Assign arguments to variables
PACKAGES = ()
KERNEL_LIMIT = 2


class Configuration:
    """
    This class groups tasks related to the program
    requirements
    """

    def __init__(self, packages):
        self.userid = os.geteuid()
        self.packages = packages

    def checkuid(self):
        """
        With this function can get the system user that are running the program
        """

        if self.userid != 0:
            sys.exit("⛔ You should be root to use this program, exiting ...")

    def requirements(self):
        """
        WIth this function can get and tried to resolve the program system requirements
        """

        result_cmd = subprocess.run(
            "dpkg -l | grep ^ii | awk '{print $2}'",
            capture_output=True,
            shell=True,
            check=False,
        )
        package_list = result_cmd.stdout.decode().split("\n")

        for package in self.packages:
            if package not in package_list:
                print(
                    f"⚠️ The package {package} is not installed\n"
                    "🔧 It will try to be installed"
                )

                result_cmd_cr = subprocess.run(
                    "apt install -y " + package,
                    capture_output=True,
                    shell=True,
                    check=False,
                )
                package_install = result_cmd_cr.stdout.decode()

                print(package_install)


class Update:
    """
    This class groups tasks related to the update
    management of the OS
    """

    def __init__(self, limit):
        self.limit = limit

    def update(self):
        """
        Whit this function can update the system totally
        """

        # pylint: disable=consider-using-with
        result_cmd_us = subprocess.Popen(
            "apt update -y && apt upgrade -qy",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            shell=True,
        )
        system_update = result_cmd_us.stdout.read()

        print("\n🔧 System updated:\n")
        print(system_update)

    def check(self):
        """
        With this function can get all available system updates
        """

        # pylint: disable=consider-using-with
        result_cmd_update = subprocess.Popen(
            "apt update -y",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            shell=True,
        )

        result_cmd_list_upgradable = subprocess.Popen(
            "apt list --upgradable",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            shell=True,
        )

        system_updatecheck = result_cmd_update.stdout.read()
        system_upgradablecheck = result_cmd_list_upgradable.stdout.read()

        print("\n🔧 System update status:\n")
        print(system_updatecheck)
        print("🔧 System upgradable packages:\n")
        print(system_upgradablecheck)

    def clean(self):
        """
        With ths function get all kernels installed on the system and
        suggest which can be removed to get system free space
        """

        result_cmd_ck = subprocess.run(
            "uname -r", capture_output=True, shell=True, check=False
        )
        current_kernel = result_cmd_ck.stdout.decode().strip()

        result_cmd_ck_gl = subprocess.run(
            f"dpkg --list | grep -E 'linux-image-[[:digit:]]' | grep -v {current_kernel} | awk '{{print $2}}' | sort",
            capture_output=True,
            shell=True,
            check=False,
        )

        kernel_installed = result_cmd_ck_gl.stdout.decode().split("\n")
        kernel_installed = list(filter(None, kernel_installed))

        if len(kernel_installed) > self.limit:
            kernel_save = kernel_installed[-1]

            print(
                f"🔦 Actually running this kernel:\n{current_kernel}\n\n"
                f"⚠️ There are more than {self.limit} kernel packages installed:\n{kernel_installed}"
            )

            # Updating the kernel list
            kernel_installed.remove(kernel_save)

            print(
                f"\n📦 This is the kernel packages candidate to remove:\n{kernel_installed}"
            )

            answer_ck = input("\nDo you want to continue? [Yy | Nn]").lower()

            if answer_ck == "y":
                print("\n🗑️ Cleaning kernel versions:")

                for kern in kernel_installed:
                    print(f"- {kern}")

                    result_cmd_ck_dpkg = subprocess.run(
                        f"apt purge -y {kern} && apt autoremove -y",
                        capture_output=True,
                        shell=True,
                        check=False,
                    )
                    kernel_uninstalled = result_cmd_ck_dpkg.stdout.decode()

                    print(kernel_uninstalled)
                    print(f"\n🔧 The kernel package {kern} was removed\n")

            else:
                sys.exit("\n✋ You've decided do not continue, exiting ...")

        else:
            print(
                f"🔦 Actually running this kernel:\n{current_kernel}\n\n"
                f"⚠️ There are not more than {self.limit} kernel packages installed:\n{kernel_installed}"
            )


# Invoke the Configuration class instance
tconfig = Configuration(packages=PACKAGES)

# Check requirements
tconfig.requirements()

# Check userid
tconfig.checkuid()

# Invoke the Credentials class instance
tupdate = Update(limit=KERNEL_LIMIT)

if args.action == "update":
    tupdate.update()

elif args.action == "check":
    tupdate.check()

elif args.action == "clean":
    tupdate.clean()
